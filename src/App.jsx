import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import { Menu } from './Components/Menu/Menu';
import { MainMenu } from './MainMenu';
import { Home } from './Pages/Home/Home';
import { Login } from './Pages/Login/Login';
import { Product } from './Pages/Product/Product';
import { Signup } from './Pages/Signup/Signup';
import { HomeRoute, LoginRoute, ProductRoutePattern, SignupRoute } from './Routes';

export const App = () => {
    return (
        <>
            <BrowserRouter>
                <Menu items={MainMenu} />
                <Switch>
                    <Route path={HomeRoute} exact component={Home} />
                    <Route path={LoginRoute} component={Login} />
                    <Route path={SignupRoute} component={Signup} />
                    <Route path={ProductRoutePattern} component={Product} />
                </Switch>
            </BrowserRouter>
        </>
    );
}