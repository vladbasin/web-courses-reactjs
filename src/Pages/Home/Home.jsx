import React, { useEffect, useState } from "react";
import { ShortProductInfo } from "../../Components/ShortProductInfo/ShortProductInfo";
import { fetchProducts } from "../../Services/fetchProducts";

export const Home = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const fetchedData = fetchProducts();

        setProducts(fetchProducts);
    }, []);

    return (
        <>
            {products.map((product, index) => 
                <ShortProductInfo 
                    key={index}
                    id={product.id}
                    title={product.title} 
                    price={product.price} 
                    description={product.description} 
                    imageUrl={product.imageUrl}
                />
            )}
        </>
    );
}