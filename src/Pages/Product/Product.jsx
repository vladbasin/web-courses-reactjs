import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { fetchSingleProduct } from "../../Services/fetchSingleProduct";

export const Product = () => {
    const { productId } = useParams();

    const [product, setProduct] = useState();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const fetchedProduct = fetchSingleProduct(productId);

        setProduct(fetchedProduct);
        setIsLoading(false);
    }, [productId]);

    return (
        <>
            {isLoading && 
                <span>Загрузка...</span>
            }
            {!isLoading &&
                <span>{product.details}</span>
            }
        </>
    );
}