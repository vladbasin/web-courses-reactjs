//value types
var number1 = 10;
var number2 = 5;

var number3 = number1;
number3 = number3 + 3;

console.log(`number1 = ${number1}`);
console.log(`number2 = ${number2}`);
console.log(`number3 = ${number3}`);

// var string1 = "Первая строка";
// var string2 = "Вторая строка";

// var string3 = string1;
// string3 = "Третья строка";

// console.log(`string1 = ${string1}`);
// console.log(`string2 = ${string2}`);
// console.log(`string3 = ${string3}`);

//reference types
const array1 = [5,4,3,2,1];
var array2 = [1,2,3,4,5];

var array3 = [...array1,...array2];
array3.push(1111);

console.log(`array1 = ${array1}`);
console.log(`array2 = ${array2}`);
console.log(`array3 = ${array3}`);