export const HomeRoute = "/";
export const LoginRoute = "/login";
export const SignupRoute = "/signup";

export const ProductRoute = "/product";
export const ProductRoutePattern = `${ProductRoute}/:productId`;