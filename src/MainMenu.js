import { HomeRoute, LoginRoute, SignupRoute } from "./Routes";

export const MainMenu = [{
    title: "Главная",
    url: HomeRoute,
}, {
    title: "Вход",
    url: LoginRoute,
}, {
    title: "Регистрация",
    url: SignupRoute,
}];