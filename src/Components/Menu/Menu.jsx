import React, { useCallback, useState } from "react";
import { Link, useLocation, useRouteMatch } from "react-router-dom";
import "./Menu.css";

export const Menu = (props) => {
    const location = useLocation();

    const getClassName = useCallback((item) => {
        if (item.url === location.pathname) {
            return "active";
        } else {
            return "";
        }
    }, [location.pathname]);

    return (
        <nav>
            <ul>
                {props.items.map(item => (
                    <li
                        key={item.url}
                        className={getClassName(item)}
                    >
                        <Link to={item.url}>{item.title}</Link>
                    </li>
                ))}
            </ul>
        </nav>
    );
}