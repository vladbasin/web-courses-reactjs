import React from "react";
import { Link } from "react-router-dom";
import { ProductRoute } from "../../Routes";
import "./ShortProductInfo.css";

export const ShortProductInfo = (props) => {
    return (
        <div className="short-product-info">
            <img className="short-product-info-logo" src={props.imageUrl} />
            <div>
                <h3>
                    <Link to={`${ProductRoute}/${props.id}`}>
                        {props.title}
                    </Link>
                </h3>
                <h4>${props.price}</h4>
                <span>{props.description}</span>
            </div>
        </div>
    );
}