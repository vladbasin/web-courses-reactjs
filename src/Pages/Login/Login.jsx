import React, { useCallback, useState } from "react";
import { ValidInput } from "../../Components/ValidInput/ValidInput";
import { FormError } from "../../Components/FormError/FormError";
import { validateEmail } from "../../Services/validateEmail";
import { validatePassword } from "../../Services/validatePassword";
import { validateCode } from "../../Services/validateCode";
import "./Login.css";

//errors[0] - ошибка для email
//errors[1] - ошибка для password
//errors[2] - ошибка для code

export const Login = () => {
    const [email, setEmail] = useState();
    const [errors, setErrors] = useState([]);

    const onValidChanged = useCallback((index, isValid) => {
        const newErrors = [...errors];
        
        if (isValid === false) {
            newErrors[index] = "Email введен неверно";
        } else {
            newErrors[index] = undefined;
        }

        setErrors(newErrors);
    }, []);

    const onEmailChanged = useCallback((text) => {
        setEmail(text);
    }, []);

    const onLogin = useCallback(() => {
        console.log(`email: ${email}, password: `);
    }, [email]);

    return (
        <>
            <div>
                <FormError errors={errors} />
                <br />
                <ValidInput 
                    placeholder="Email" 
                    validate={validateEmail}
                    onChanged={onEmailChanged}
                    onValidChanged={isValid => onValidChanged(0, isValid)}
                />
                <br />
                <ValidInput 
                    placeholder="Пароль" 
                    validate={validatePassword}
                    onValidChanged={isValid => onValidChanged(1, isValid)}
                />
                <br />
                <ValidInput 
                    placeholder="6+4" 
                    validate={validateCode}
                    onValidChanged={isValid => onValidChanged(2, isValid)}
                />
                <br />
                <input type="submit" value="Войти" onClick={onLogin} />
            </div>
        </>
    );
}