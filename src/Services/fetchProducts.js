import IPhone12Pro from "../Assets/Images/iphone-12-pro.png";
import IPhone12 from "../Assets/Images/iphone-12.png";
import GooglePixel from "../Assets/Images/google-pixel.jpg";

export const fetchProducts = () => {
    return [{
        id: "1",
        title: "iPhone 12",
        price: "1200",
        description: "Новый айфон",
        imageUrl: IPhone12,
        details: "Apple iOS, экран 6.1\" OLED (1170x2532), Apple A14 Bionic, ОЗУ 4 ГБ, флэш-память 256 ГБ, камера 12 Мп, аккумулятор 2815 мАч, 1 SIM",
    }, {
        id: "2",
        title: "iPhone 12 PRO",
        price: "1800",
        description: "Новый айфон для мажоров",
        imageUrl: IPhone12Pro,
        details: "Apple iOS, экран 6.1\" OLED (1170x2532), Apple A14 Bionic, ОЗУ 6 ГБ, флэш-память 128 ГБ, камера 12 Мп, аккумулятор 2775 мАч, 1 SIM",
    }, {
        id: "3",
        title: "Google Pixel 32Gb",
        price: "100",
        description: "Смартфон на Android",
        imageUrl: GooglePixel,
        details: "Android, экран 5\" AMOLED (1080x1920), Qualcomm Snapdragon 821 MSM8996Pro AB, ОЗУ 4 ГБ, флэш-память 32 ГБ, камера 12.3 Мп, аккумулятор 2770 мАч, 1 SIM",
    }];
}